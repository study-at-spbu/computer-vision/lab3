from PanoramaTrasform.ImagePanoramaTrasform import ImageLoader
from PanoramaTrasform.MergePanoramaTrasform import ImageMerger
from PanoramaTrasform.PanoramaTrasform import ImageTrasform
from SliceType import SliceType


class ImageMergerManager:
    _transform: ImageTrasform

    def __init__(self):
        self._transform = None

    def merge(self, arg, slice_type: SliceType):
        if isinstance(arg, ImageMergerManager):
            return self._mergeTransform(arg, slice_type)

        return self._mergeImage(arg, slice_type)

    def _mergeImage(self, image_str: str, slice_type: SliceType):
        image_transform = ImageLoader(image_str)
        manager = ImageMergerManager()
        if self._transform is None:
            transform = image_transform
        else:
            transform = ImageMerger(self._transform, image_transform, slice_type)
        manager._setTransform(transform)
        return manager

    def _mergeTransform(self, tranform, slice_type: SliceType):
        if self._transform is None:
            return tranform
        manager = ImageMergerManager()
        manager._setTransform(ImageMerger(self._transform, tranform._transform, slice_type))
        return manager

    def _setTransform(self, transform: ImageTrasform):
        self._transform = transform

    def getImage(self):
        return self._transform.action()[0]
