from abc import ABC, abstractmethod

import cv2
import numpy as np
from numpy.polynomial import Polynomial

from type_hiting import Image


class SliceClass(ABC):
    @abstractmethod
    def _sliceImage(self, image: Image, x) -> Image:
        raise NotImplementedError()

    @abstractmethod
    def _getLinePoints(self, points):
        raise NotImplementedError()

    @abstractmethod
    def _getNearestX(self, arr: list[int]) -> int:
        raise NotImplementedError()

    def compare(self, stable_image: Image, translation_dist, output_img: Image, orig_point, merged_image: Image):
        rows1: int = stable_image.shape[0]
        result: Image = self._sliceToHorizonLine(translation_dist, output_img, rows1)
        result = self._deleteEmptySpace(result)
        x: int = self.new_method(translation_dist, orig_point, rows1)
        if x < result.shape[1]:
            result = self.n_sliceImage(result, x)

        result = self._deleteEmptySpace(result)
        result = self._dropToBound(stable_image, merged_image, result)

        return result

    def _sliceToHorizonLine(self, translation_dist, output_img: Image, rows1: int):
        return output_img[translation_dist[1]:rows1 + translation_dist[1]]

    def _dropToBound(self, stable_image: Image, merged_image: Image, result: Image):
        if result.shape[1] <= int((stable_image.shape[1] + merged_image.shape[1]) * 2):
            return result

        x = int((stable_image.shape[1] + merged_image.shape[1]) * 2)
        result = self._sliceImage(result, x)
        return result

    def _deleteEmptySpace(self, result: Image):
        gray: Image = cv2.cvtColor(result, cv2.COLOR_BGR2GRAY)
        _, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)
        cnts, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        c = max(cnts, key=cv2.contourArea)
        (x, y, w, h) = cv2.boundingRect(c)
        result = result[y:y + h, x:x + w]
        return result

    def new_method(self, translation_dist, orig_point, rows1):
        line_points = np.flip(self._getLinePoints(orig_point), axis=1)
        points = np.vstack(line_points)
        points = np.transpose(points)

        coefficients: Polynomial = Polynomial.fit(points[0], points[1], 1)

        x1: int = round(coefficients(translation_dist[1]))
        x2: int = round(coefficients(translation_dist[1] + rows1))
        x: int = self._getNearestX([x1, x2])
        x = max(0, x)

        return x
