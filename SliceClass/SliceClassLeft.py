import numpy as np

from SliceClass.SliceClass import SliceClass
from type_hiting import Image


class SliceClassLeft(SliceClass):
    def _sliceImage(self, image: Image, x) -> Image:
        return image[:, -x:]

    def n_sliceImage(self, image: Image, x) -> Image:
        return image[:, x:]

    def _getLinePoints(self, points):
        return np.array([points[0][0], points[1][0]])

    def _getNearestX(self, arr: list[int]) -> int:
        return max(arr)
