import numpy as np

from SliceClass.SliceClass import SliceClass
from type_hiting import Image


class SliceClassRight(SliceClass):
    def _sliceImage(self, image: Image, x) -> Image:
        return image[:, :x]

    def n_sliceImage(self, image: Image, x) -> Image:
        return image[:, :x]

    def _getLinePoints(self, points):
        return np.array([points[2][0], points[3][0]])

    def _getNearestX(self, arr: list[int]) -> int:
        return min(arr)
