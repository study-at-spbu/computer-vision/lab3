import cv2
import numpy as np

from PanoramaTrasform.PanoramaTrasform import ImageTrasform
from SliceClass.SliceClassLeft import SliceClassLeft
from SliceClass.SliceClassRight import SliceClassRight
from SliceType import SliceType
from type_hiting import Image


class ImageMerger(ImageTrasform):
    image1: Image
    image2: Image
    _slice_type: SliceType

    def __init__(self, merged_image_transform: ImageTrasform, stable_image_transform: ImageTrasform,
                 slice_type: SliceType):
        self._stable_image_transform = stable_image_transform
        self._merged_image_transform = merged_image_transform
        self._slice_type = slice_type

    def warpImages(self, stable_image: Image, merged_image: Image, homography):
        rows1, cols1 = stable_image.shape[:2]
        rows2, cols2 = merged_image.shape[:2]

        list_of_points_1 = np.float32([[0, 0], [0, rows1], [cols1, rows1], [cols1, 0]]).reshape(-1, 1, 2)
        temp_points = np.float32([[0, 0], [0, rows2], [cols2, rows2], [cols2, 0]]).reshape(-1, 1, 2)

        list_of_points_2 = cv2.perspectiveTransform(temp_points, homography)

        list_of_points = np.concatenate((list_of_points_1, list_of_points_2), axis=0)

        [x_min, y_min] = np.int32(list_of_points.min(axis=0).ravel() - 0.5)
        [x_max, y_max] = np.int32(list_of_points.max(axis=0).ravel() + 0.5)

        translation_dist = [-x_min, -y_min]

        translation_dist[0] += max(0, int(list_of_points_2[0][0][0] - list_of_points_2[3][0][0]))
        translation_dist[1] += max(0, int(list_of_points_2[0][0][1] - list_of_points_2[3][0][1]))

        homography_translation = np.array(
            [[1, 0, translation_dist[0]], [0, 1, translation_dist[1]], [0, 0, 1]])

        output_img: Image = cv2.warpPerspective(merged_image, homography_translation.dot(homography), (
            x_max - x_min + rows1 + translation_dist[0], y_max - y_min + cols1 + translation_dist[1]))
        output_img[translation_dist[1]:rows1 + translation_dist[1],
        translation_dist[0]:cols1 + translation_dist[0]] = stable_image

        orig_point = cv2.perspectiveTransform(temp_points, homography_translation.dot(homography))
        if self._slice_type == SliceType.LEFT2RIGHT:
            slice_class = SliceClassLeft()
        else:
            slice_class = SliceClassRight()
        result = slice_class.compare(stable_image, translation_dist, output_img, orig_point, merged_image)

        return result, stable_image.shape[1]

    def matchKeypoints(self, stable_image_descriptors, merged_image_descriptors):
        matcher: cv2.BFMatcher = cv2.BFMatcher(cv2.NORM_L2, crossCheck=False)
        matches = matcher.knnMatch(stable_image_descriptors, merged_image_descriptors, 2)
        result_matches = []
        for m, n in matches:
            if m.distance < n.distance * 0.6:
                result_matches.append(m)

        return result_matches

    def _createMask(self, image, effective_search_width):
        mask: Image = np.zeros(image.shape[:2], dtype='uint8')

        if self._slice_type == SliceType.LEFT2RIGHT:
            mask[:, -effective_search_width:] = 255
        else:
            mask[:, :effective_search_width] = 255

        return mask

    def getHomography(self, stable_image_keypoints, merged_image_keypoints, matches):

        stable_image_keypoints = np.float32([kp.pt for kp in stable_image_keypoints])
        merged_image_keypoints = np.float32([kp.pt for kp in merged_image_keypoints])

        if len(matches) <= 4:
            raise Exception("Error!")

        stable_image_points = np.float32([stable_image_keypoints[m.queryIdx] for m in matches])
        merged_image_points = np.float32([merged_image_keypoints[m.trainIdx] for m in matches])

        homography, _ = cv2.findHomography(stable_image_points, merged_image_points, cv2.RANSAC, 5)

        return homography

    def action(self):
        stable_image, _ = self._stable_image_transform.action()
        merged_image, merged_effective_search_width1 = self._merged_image_transform.action()
        stable_image_gray = cv2.cvtColor(stable_image, cv2.COLOR_RGB2GRAY)
        merged_image_gray = cv2.cvtColor(merged_image, cv2.COLOR_RGB2GRAY)
        mask = self._createMask(merged_image, merged_effective_search_width1)

        obs = cv2.SIFT_create()

        stable_image_keypoints, stable_image_descriptors = obs.detectAndCompute(merged_image_gray, mask=mask)
        merged_image_keypoints, merged_image_descriptors = obs.detectAndCompute(stable_image_gray, None)

        matches = self.matchKeypoints(stable_image_descriptors, merged_image_descriptors)

        homography = self.getHomography(stable_image_keypoints, merged_image_keypoints, matches)

        result, effective_search_width = self.warpImages(stable_image, merged_image, homography)
        return result, effective_search_width
