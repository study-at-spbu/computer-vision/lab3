from pathlib import Path

import cv2

from PanoramaTrasform.PanoramaTrasform import ImageTrasform


class ImageLoader(ImageTrasform):
    def __init__(self, path: str):
        self._path = Path(path)
        super().__init__()

    def action(self):
        image = cv2.imread(str(self._path.resolve()))
        return image, image.shape[1]
