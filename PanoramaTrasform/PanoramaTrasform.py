from abc import ABC, abstractmethod


class ImageTrasform(ABC):
    @abstractmethod
    def action(self):
        raise NotImplementedError()
