import cv2

from ImageMergerManager import ImageMergerManager
from SliceType import SliceType

BUILDING1_IMAGES: list[str] = [
    'building1/building1.jpg',
    'building1/building2.jpg',
    'building1/building3.jpg',
    'building1/building4.jpg',
    'building1/building5.jpg'
]

BUILDING2_IMAGES: list[str] = [
    'building2/dsc_2711.jpg',
    'building2/dsc_2710.jpg',
    'building2/dsc_2709.jpg',
    'building2/dsc_2708.jpg',
    'building2/dsc_2707.jpg'
]

BUILDING3_IMAGES: list[str] = [
    'building3/dsc_2727.jpg',
    'building3/dsc_2726.jpg',
    'building3/dsc_2725.jpg',
    'building3/dsc_2724.jpg',
    'building3/dsc_2723.jpg',
    'building3/dsc_2722.jpg',
    'building3/dsc_2721.jpg'
]


def createTransform(images: list[str], root_image_id: int = None):
    if root_image_id == None:
        root_image_id = len(images) // 2

    transform_left: ImageMergerManager = ImageMergerManager()
    for i in range(root_image_id + 1):
        transform_left = transform_left.merge(
            images[i],
            SliceType.LEFT2RIGHT
        )

    transform_right: ImageMergerManager = ImageMergerManager()
    for i in range(len(images) - 1, root_image_id, -1):
        transform_right = transform_right.merge(
            images[i],
            SliceType.RIGHT2LEFT
        )

    return transform_right.merge(
        transform_left,
        SliceType.RIGHT2LEFT
    )




for i, images in enumerate([BUILDING1_IMAGES, BUILDING2_IMAGES, BUILDING3_IMAGES], 1):
    transform = createTransform(images)
    result_image = transform.getImage()
    cv2.imwrite(f'result{i}.png', result_image)


