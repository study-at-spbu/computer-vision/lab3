from enum import IntEnum


class SliceType(IntEnum):
    LEFT2RIGHT = 1
    RIGHT2LEFT = 2
